import {NgModule} from '@angular/core';

import {AuthRoutingModule} from './auth-routing.module';
import {NB_AUTH_TOKEN_INTERCEPTOR_FILTER, NbAuthJWTToken, NbAuthModule, NbPasswordAuthStrategy} from '@nebular/auth';

import {HttpClientModule, HttpRequest} from '@angular/common/http';
import {NbRoleProvider, NbSecurityModule} from '@nebular/security';
import {AuthGuard} from './auth-guard.service';
import {AuthService} from './auth.service';
import {RoleGuard} from './role-guard.service';
import {RoleProvider} from './role.provider';

export function filterInterceptorRequest(req: HttpRequest<any>) {
  return [].some(url => req.url.includes(url));
}

@NgModule({
  imports: [

    HttpClientModule,
    AuthRoutingModule,
    NbAuthModule.forRoot({
      strategies: [
        NbPasswordAuthStrategy.setup({
          name: 'email',
          token: {
            class: NbAuthJWTToken,
            key: 'token'
          },
          baseEndpoint: 'http://localhost:3000',
          login: {
            requireValidToken: false,
            endpoint: '/api/auth/login',
            method: 'post',
            redirect: {
              success: 'admin',
              failure: null
            },
            defaultErrors: [
              'Login/Email combination is not correct, please try again.'
            ],
            defaultMessages: ['You have been successfully logged in.'],
          },
          logout: {endpoint: '', redirect: {success: 'auth/login', failure: 'auth/login'}},
        })
      ]
    }),
    NbSecurityModule.forRoot({
      accessControl: {
        'ADMIN': {
          view: '*',
          create: '*',
          remove: '*'
        }
      },
    })
  ],
  providers: [
    AuthGuard,
    AuthService,
    RoleGuard,
    {
      provide: NB_AUTH_TOKEN_INTERCEPTOR_FILTER,
      useValue: filterInterceptorRequest
    },
    {provide: NbRoleProvider, useClass: RoleProvider}
  ],
  exports: [NbAuthModule]
})
export class AuthModule {
}
