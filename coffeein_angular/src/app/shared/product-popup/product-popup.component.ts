import {Component, Input, OnInit} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-product-popup',
  templateUrl: './product-popup.component.html',
  styleUrls: ['./product-popup.component.scss']
})
export class ProductPopupComponent implements OnInit {
  @Input() color;
  @Input() slug;
  @Input() totalPrice;
  @Input() countProducts;
  @Input() value_language;
  display = true;


  constructor(private route: Router) {
  }

  getCompany(company) {
    this.route.navigate(['/cart', company]);
  }

  ngOnInit(): void {

    if (this.countProducts > 99) {
      this.display = false;
    }
  }

}
