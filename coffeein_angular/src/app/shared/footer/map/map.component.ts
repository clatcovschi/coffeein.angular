import {Component, Input, OnInit} from '@angular/core';
import * as Leaflet from "leaflet"
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {
  @Input() name_map;
  @Input() display_popup;
  @Input() latitude;
  @Input() longitude;
  @Input() message;
  title = 'leafletApps';
  map: Leaflet.Map;
  company_slug;
  company_logo;
  map_data;
  greenIcon;

  constructor(private http: HttpClient, private route: Router) {
  }

  ngOnInit(): void {
    this.company_slug = this.route.url.split('/');

    this.http.get<any>('http://localhost:3000/api/companies').subscribe(company => {
      for (let i = 0; i < company.docs.length; i++) {

        if (company.docs[i].slug === this.company_slug[2]) {
          this.company_logo = company.docs[i];

          this.map_data = company.docs[i].coordinates.split(',');

          this.map = Leaflet.map(this.name_map).setView([this.latitude, this.longitude], 10);
          Leaflet.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(this.map);
          if (this.display_popup) {
            this.greenIcon = Leaflet.icon({
              iconUrl: 'http://coffeein.md/assets/img/icons/map-marker.png',

              iconSize: [16, 16],
              popupAnchor: [0, -8]
            });


            Leaflet.marker([this.latitude, this.longitude], {icon: this.greenIcon}).addTo(this.map).bindPopup(this.message).openPopup()
          }

        }
      }
    });
  }

}
