import {Component, OnInit, TemplateRef} from '@angular/core';
import {NbDialogModule, NbDialogRef, NbDialogService, NbToastrService} from "@nebular/theme";
import {HttpClient} from "@angular/common/http";
import {AccessTokenService} from "../../consts/access-token.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Observable} from "rxjs";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  registerForm: FormGroup;
  submitted = false;
  response;
  columns: {};

  constructor(private formBuilder: FormBuilder,
              private dialogService: NbDialogService,
              private toastrService: NbToastrService,
              private http: HttpClient,
              public accessToken: AccessTokenService) {
  }


  open(dialog: TemplateRef<any>) {
    this.dialogService.open(dialog, {});
  }


  ngOnInit() {
    this.columns = {
      _id: {
        title: 'ID'
      },
      role: {
        title: 'Role'
      },
      email: {
        title: 'Email'
      },
      lastName: {
        title: 'Last Name'
      },

    }
    this.http.get('http://localhost:3000/api/users', {headers: this.accessToken.headers}).subscribe(data => {
      this.response = data;
    })
    this.registerForm = this.formBuilder.group({
      firstName: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(50)]],
      lastName: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(50)]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  get f() {
    return this.registerForm.controls;
  }

  onSubmit(): Observable<any> {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    } else {
      this.http.post('http://localhost:3000/api/users', this.registerForm.value, {headers: this.accessToken.headers}).subscribe(() => {
        this.showToast('top-right', 'success');
        this.http.get('http://localhost:3000/api/users', {headers: this.accessToken.headers}).subscribe(data => {
          this.response = data;
        })
      })

    }
  }

  showToast(position, status) {
    this.toastrService.show(
      status || 'success',
      `User added successfully`,
      {position, status});
  }

}
