import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {AccessTokenService} from "../../consts/access-token.service";
import {ActivatedRoute} from "@angular/router";
import {CookieService} from "ngx-cookie-service";

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {
  columns: {};
  language = 'ro';
  lg;
  response;
  id;
  object;


  constructor(private http: HttpClient,
              public accessToken: AccessTokenService,
              private route: ActivatedRoute,
              private cookieService: CookieService) {
    route.queryParams.subscribe(p => {
      this.id = p.Id;
      if (this.id !== undefined) {
        this.http.get(`http://localhost:3000/api/order-request/${this.id}`, {headers: this.accessToken.headers}).subscribe(data => {
          // @ts-ignore
          this.object = data;
        });
      }
    });
  }

  ngOnInit(): void {
    if (this.cookieService.get('lang') !== '') {
      this.language = this.cookieService.get('lang');
    }
    this.http.get<any>('http://localhost:3000/api/languages?limit=9999999999999999999').subscribe(data => {
      for (let i = 0; i < data.docs.length; i++) {
        if (data.docs[i].slug === this.language) {
          this.lg = i;
        }
      }
    });
    this.columns = {
      id: {
        title: 'ID'
      },
      phone: {
        title: 'PHONE'
      },
      fullName: {
        title: 'FULL NAME'
      },
      totalPrice: {
        title: 'TOTAL PRICE'
      }
    }

    this.http.get<any>('http://localhost:3000/api/order-request?limit=99999999999', {headers: this.accessToken.headers}).subscribe(res => {
      this.response = res.docs;
    });

  }

}
