import {Component, OnInit, TemplateRef} from '@angular/core';
import {NbDialogService} from '@nebular/theme';
import {HttpClient} from "@angular/common/http";
import {AccessTokenService} from "../../consts/access-token.service";
import {ActivatedRoute, Router} from "@angular/router";
import {TokenInterceptor} from "../../admin-token-interceptor";

@Component({
  selector: 'app-create-company',
  templateUrl: './create-company.component.html',
  styleUrls: ['./create-company.component.scss']
})
export class CreateCompanyComponent implements OnInit {
  languages = [];
  columns: {}
  response;
  data;
  file;
  color;
  isEditPage = false;
  isMainCompany = false;
  image_src = {};
  imageUpload;
  logo_src = {};
  id;
  editLogo = [];
  editImage = [];
  object = {
    name: '',
    phone: '',
    slug: '',
    mail: '',
    coordinates: '',
    color: '',
    slogan: [],
    isMainCompany: this.isMainCompany,
    instagram: '',
    facebook: '',
    youtube: '',
    logo: {},
    mainProductImage: {}
  };

  constructor(private dialogService: NbDialogService,
              private http: HttpClient,
              public accessToken: AccessTokenService,
              private route: ActivatedRoute,
              private router: Router) {
    this.file = []
    route.queryParams.subscribe(p => {
      this.editLogo = []
      this.editImage = []
      this.id = p.Id;
      if (this.id !== undefined) {
        this.isEditPage = true;
        this.http.get(`http://localhost:3000/api/companies/${this.id}`).subscribe(data => {
          // @ts-ignore
          this.object = data;
          // @ts-ignore
          if (data.logo && data.logo.path) {
            // @ts-ignore
            this.logo_src = data.logo;
            // @ts-ignore
            this.editLogo = [{url: 'http://localhost:3000/api/' + data.logo.path.replaceAll('\\', '/'), fileName: data.logo.description
            }];
          }
          // @ts-ignore
          if (data.mainProductImage && data.mainProductImage.path) {
            // @ts-ignore
            this.image_src = data.mainProductImage;
            // @ts-ignore
            this.editImage = [{url: 'http://localhost:3000/api/' + data.mainProductImage.path.replaceAll('\\', '/'), fileName: data.mainProductImage.description
            }];

          }


        });
      }
    });

  }

  open(dialog: TemplateRef<any>) {
    this.editLogo = []
    this.editImage = []
    this.image_src = [];
    this.logo_src = [];
    this.dialogService.open(dialog, {});
    this.router.navigate(
      ['.'],
      {relativeTo: this.route}
    );
    this.isEditPage = false;
    this.object = {
      name: '',
      phone: '',
      slug: '',
      mail: '',
      coordinates: '',
      color: '',
      slogan: [],
      isMainCompany: false,
      instagram: '',
      facebook: '',
      youtube: '',
      logo: {},
      mainProductImage: {}
    };
    this.http.get<any>('http://localhost:3000/api/languages?limit=9999999999999999999').subscribe(data => {
      for (let i = 0; i < data.docs.length; i++) {
        this.object.slogan.push({shortName: data.docs[i].slug, content: ''});
      }
    });
  }

  ngOnInit(): void {


    this.http.get('http://localhost:3000/api/companies?limit=9999999999999999999').subscribe(data => {
      // @ts-ignore
      this.response = data.docs;
    });
    this.imageUpload = 'http://localhost:3000/api/upload/company/image';

    this.columns = {
      id: {
        title: 'ID'
      },
      name: {
        title: 'NAME'
      },
      mail: {
        title: 'MAIL'
      }
    }


    this.http.get<any>('http://localhost:3000/api/languages?limit=9999999999999999999').subscribe(data => {
      for (let i = 0; i < data.docs.length; i++) {
        this.languages[i] = data.docs[i].slug;
      }
    });
  }

  mainCompany(event) {
    this.isMainCompany = event;
  }

  onSubmit() {
    this.object.logo = this.logo_src;
    this.object.mainProductImage = this.image_src;


    if (this.isEditPage) {
      this.http.put(`http://localhost:3000/api/companies/${this.id}`, JSON.stringify(this.object), {headers: this.accessToken.headers}).subscribe(res => {
        this.http.get('http://localhost:3000/api/companies?limit=9999999999999999999').subscribe(data => {
          // @ts-ignore
          this.response = data.docs;


        });
        this.http.get(`http://localhost:3000/api/companies/${this.id}`).subscribe(data => {
          // @ts-ignore
          this.editLogo = [{url: 'http://localhost:3000/api/' + data.logo.path.replaceAll('\\', '/'), fileName: data.logo.description
          }];
          // @ts-ignore
          this.editImage = [{url: 'http://localhost:3000/api/' + data.mainProductImage.path.replaceAll('\\', '/'), fileName: data.mainProductImage.description
          }];

        });
      });
    } else {
      this.http.post(`http://localhost:3000/api/companies`, JSON.stringify(this.object), {headers: this.accessToken.headers}).subscribe(res => {
        this.http.get('http://localhost:3000/api/companies?limit=9999999999999999999').subscribe(data => {
          // @ts-ignore
          this.response = data.docs;
        });
      });
    }


  }

  uploadFinished(event, isLogo) {
    if (isLogo) {
      if (event && event.serverResponse && event.serverResponse.response && event.serverResponse.response.body) {
        this.logo_src = {
          path: event.serverResponse.response.body.path.replaceAll('\\', '/'),
          mimetype: event.serverResponse.response.body.mimetype,
          description: event.serverResponse.response.body.filename
        };
      }
    } else {
      if (event && event.serverResponse && event.serverResponse.response && event.serverResponse.response.body) {
        this.image_src = {
          path: event.serverResponse.response.body.path.replaceAll('\\', '/'),
          mimetype: event.serverResponse.response.body.mimetype,
          description: event.serverResponse.response.body.filename
        };
      }
    }

  }

  removeImage() {
    this.image_src = [];
  }

  removeLogo() {
    this.logo_src = [];
  }
}
