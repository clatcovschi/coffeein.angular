import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';

@Injectable()
export class AdminUserProfileService {
  constructor(private httpClient: HttpClient) {
  }

  getCurrentProfile() {
    return this.httpClient.get('http://localhost:3000' + '/api/profile');
  }
}
