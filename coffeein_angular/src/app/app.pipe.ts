import {Pipe, PipeTransform} from "@angular/core";

@Pipe({
  name: 'imageUrl'
})

export class AppPipe implements PipeTransform {
  apiStorage = 'http://localhost:3000/api/';

  transform(value: string): any {
    return this.apiStorage + value;
  }
}






