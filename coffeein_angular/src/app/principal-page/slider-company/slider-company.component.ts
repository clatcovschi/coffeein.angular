import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CookieService} from "ngx-cookie-service";

@Component({
  selector: 'app-slider-company',
  templateUrl: './slider-company.component.html',
  styleUrls: ['./slider-company.component.scss']
})
export class SliderCompanyComponent implements OnInit {

  slides;
  language;
  lg;
  slideConfig = {
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    arrows: true,
    dots: true,
    infinite: true,
    autoplaySpeed: 1500,
    speed: 1000,
    responsive: [
      {
        breakpoint: 767,
        settings: {
          arrows: false
        }

      }
    ]
  };


  breakpoint(e) {
  }

  constructor(private http: HttpClient,
              private cookieService: CookieService) {
  }

  ngOnInit(): void {
    if (this.cookieService.get('lang') !== '') {
      this.language = this.cookieService.get('lang');
    } else {
      this.language = 'ro';
      this.cookieService.set('lang', 'ro');
    }
    this.http.get<any>('http://localhost:3000/api/slider-info').subscribe(data => {
      this.slides = data.docs;
    });

    this.http.get<any>('http://localhost:3000/api/languages').subscribe(data => {
      for (let i = 0; i < data.docs.length; i++) {
        if (data.docs[i].slug === this.language) {
          this.lg = i;
        }
      }
    });
  }

}
