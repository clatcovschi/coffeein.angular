import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {CookieService} from "ngx-cookie-service";

@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.scss'],
})
export class CompaniesComponent implements OnInit {
  docs;
  phone;
  language;
  lg;
  nr_products;
  main_company;
  value_language;
  languages = [];
  main_api_url = 'http://localhost:3000/api/companies';
  private slug: number;

  constructor(private http: HttpClient,
              private router: Router,
              private cookieService: CookieService
  ) {
  }

  getLanguage(lang) {
    this.cookieService.set('lang', lang);
    window.location.reload();
    this.language = lang;
  }

  getCompany(company) {
    this.router.navigate(['/products', company]);
  }

  getCart() {

    this.router.navigate(['/cart', this.main_company]);
  }


  ngOnInit() {
    if (this.cookieService.get('lang') !== '') {
      this.language = this.cookieService.get('lang');
    } else {
      this.language = 'ro';
      this.cookieService.set('lang', 'ro');
    }
    if (this.cookieService.get('countProducts') !== null) {
      if (Number(this.cookieService.get('countProducts')) <= 99) {
        this.nr_products = Number(this.cookieService.get('countProducts'));
      } else {
        this.nr_products = '99+';
      }
    } else {
      this.nr_products = 0;
    }


    this.http.get<any>(this.main_api_url).subscribe(data => {
      this.docs = data.docs;
      for (let i = 0; i < data.docs.length; i++) {
        if (data.docs[i].isMainCompany === true) {
          this.phone = data.docs[i].phone;
          this.main_company = data.docs[i].slug;
        }
      }
    });
    this.http.get<any>('http://localhost:3000/api/languages').subscribe(data => {
      for (let i = 0; i < data.docs.length; i++) {
        this.languages[i] = data.docs[i].slug;
        if (data.docs[i].slug === this.language) {
          this.lg = i;
        }
      }
    });

    this.http.get(`../assets/languages/${this.language}.json`).subscribe(data => {

      this.value_language = data;

    });
  }
}

