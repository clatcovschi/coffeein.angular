import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {CommonModule} from '@angular/common';
import {AppRoutingModule} from './app-routing.module';
import {FetchJsonPipe} from "./fetch-json.pipe";
import {AppComponent} from './app.component';
import {CompaniesComponent} from './principal-page/companies/companies.component';
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  NbThemeModule,
  NbLayoutModule,
  NbInputModule,
  NbCheckboxModule,
  NbAlertModule,
  NbDialogModule, NbCardModule, NbButtonModule, NbToastrModule, NbSelectModule
} from "@nebular/theme";
import {NbEvaIconsModule} from '@nebular/eva-icons';
import {SliderCompanyComponent} from './principal-page/slider-company/slider-company.component';
import {SlickCarouselModule} from "ngx-slick-carousel";
import {RouterModule} from "@angular/router";
import {CompanyComponent} from './extract-data/company/company.component';
import {CartComponent} from './extract-data/cart/cart.component';
import {PrincipalPageComponent} from './principal-page/principal-page.component';
import {ExtractDataComponent} from './extract-data/extract-data.component';
import {AppPipe} from "./app.pipe";
import {SharedComponent} from './shared/shared.component';
import {FooterComponent} from './shared/footer/footer.component';
import {NavbarCompanyComponent} from './shared/navbar-company/navbar-company.component';
import {ProductPopupComponent} from './shared/product-popup/product-popup.component';
import {SendPopupComponent} from './shared/send-popup/send-popup.component';
import {MapComponent} from './shared/footer/map/map.component';
import {FormBuilder, FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NotFoundPageComponent} from './not-found-page/not-found-page.component';
import {CookieService} from "ngx-cookie-service";
import {NbAuthModule, NbDummyAuthStrategy} from "@nebular/auth";
import {ImageUploadModule} from "angular2-image-upload";
import {AccessTokenService} from "./admin/consts/access-token.service";


@NgModule({
  declarations: [
    AppComponent,
    CompaniesComponent,
    FetchJsonPipe,
    AppPipe,
    SliderCompanyComponent,
    CompanyComponent,
    CartComponent,
    PrincipalPageComponent,
    ExtractDataComponent,
    SharedComponent,
    FooterComponent,
    NavbarCompanyComponent,
    ProductPopupComponent,
    SendPopupComponent,
    MapComponent,
    NotFoundPageComponent,
  ],
  imports: [
    ImageUploadModule,
    NbAuthModule.forRoot({
      strategies: [
        NbDummyAuthStrategy.setup({
          name: 'email',

          alwaysFail: true,
          delay: 1000,
        }),
      ],
    }),
    CommonModule,
    BrowserModule,
    NgbModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NbLayoutModule,
    AppRoutingModule,
    NbSelectModule,
    NbDialogModule.forRoot(),
    NbToastrModule.forRoot(),
    RouterModule.forRoot([
      {path: '', component: PrincipalPageComponent},
      {path: 'products/:id', component: CompanyComponent},
      {path: 'cart/:id', component: CartComponent},
      {path: '**', component: NotFoundPageComponent},
    ]),

    NbEvaIconsModule,
    NbCardModule,
    SlickCarouselModule,
    NbInputModule,
    NbButtonModule,
    NbCheckboxModule,
    FormsModule,
    NbThemeModule.forRoot({name: 'default'}),
    NbAlertModule,
    ReactiveFormsModule,



  ],
  providers: [CookieService, AccessTokenService, FormBuilder
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
