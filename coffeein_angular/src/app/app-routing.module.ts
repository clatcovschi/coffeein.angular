import {NgModule} from '@angular/core';

import {Routes, RouterModule} from '@angular/router';
import {AuthGuard} from "./auth/auth-guard.service";


export const routes: Routes = [
  {
    path: 'admin',
    canActivate: [AuthGuard],
    data: {
      expectedRole: 'admin'
    },
    loadChildren: () => import('./admin/admin.module')
      .then(m => m.AdminModule),
  },
  {
    path: 'auth',
    loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
